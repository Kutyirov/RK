#include <iostream>
#include <string>
using namespace std;
int main() {
	int raz, fl = 0, k = 0;
	cin >> raz;
	char c;
	cin >> c;
	if (raz <= 0) fl = 1;
	if (fl == 0) {
		int* a = new int[raz];
		for (int i = 0; i < raz; i++) {
			a[i] = 0;
		}
		while (c != '\n') {
			if (c == ' ') {
				k++;
			}
			else {
				a[k] = a[k] * 10 + c - 48;
			}
			cin.get(c);
		}
		int sd;
		cin >> sd;
		if ((sd < 0) || (k != raz - 1)) fl = 1;
		sd %= raz;
		if (fl == 0) {
			for (int i = 0; i < (raz - sd) / 2; i++)
				swap(a[i], a[raz - sd - 1 - i]);

			for (int i = raz - sd; i < (2 * raz - sd) / 2; i++) {
				swap(a[i], a[2 * raz - sd - i - 1]);
			}

			for (int i = 0; i < raz / 2; i++)
				swap(a[i], a[raz - 1 - i]);

			for (int i = 0; i < raz; i++)
				cout << a[i] << " ";
		}
		else
			cout << "An error has occurred while reading input data";
		delete[] a;
	}
	cin.get();
	cin.get();
}
