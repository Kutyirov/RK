#include <iostream>
using namespace std;
int main() {
	int row = -1, col = -1, fl = 0, str = 0, sd, k, k_sp = 0;;
	char c;
	cin >> row;
	cin.get(c);
	cin >> col;
	if ((row <= 0) || (col <= 0)) fl = 1;
	if (fl == 0) {
		int** a = new int*[row];
		for (int i = 0; i < row; i++)
			a[i] = new int[col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				a[i][j] = 0;
			}
		}
		while (str < row) {
			cin >> c;
			k = 0;
			while (c != '\n') {
				if (c == ' ') {
					k++;
				}
				else {
					a[str][k] = a[str][k] * 10 + c - 48;
				}
				cin.get(c);

			}
			if (k != col - 1) fl = 1;
			str++;
		}
		if (row < col) 
			k_sp = row / 2; 
		else 
			k_sp = col / 2;
			
		cin >> sd;
		if (sd < 0) fl = 1;
		if (fl == 0) {
			for (int sp = 0; sp < k_sp; sp++) {
				int size = (col + row - 2 - sp*4) * 2, num = 0, rw = sp, cl = sp-1;
				sd %= size;
				int* b = new int[size];
				while ((num < size) && (cl + 1 < col - sp)) {
					cl++;
					b[num] = a[rw][cl];
					num++;
				}
				while ((num < size) && (rw + 1 < row - sp)) {
					rw++;
					b[num] = a[rw][cl];
					num++;
				}
				while ((num < size) && (cl - 1 >= sp)) {
					cl--;
					b[num] = a[rw][cl];
					num++;
				}
				while ((num < size) && (rw - 1 >= sp)) {
					rw--;
					b[num] = a[rw][cl];
					num++;
				}
				for (int i = 0; i < (size - sd) / 2; i++)
					swap(b[i], b[size - sd - 1 - i]);

				for (int i = size - sd; i < (2 * size - sd) / 2; i++) {
					swap(b[i], b[2 * size - sd - i - 1]);
				}

				for (int i = 0; i < size / 2; i++)
					swap(b[i], b[size - 1 - i]);

				num = 0, rw = sp, cl = sp-1;
				while ((num < size) && (cl + 1 < col - sp)) {
					cl++;
					a[rw][cl] = b[num];
					num++;
				}
				while ((num < size) && (rw + 1 < row - sp)) {
					rw++;
					a[rw][cl] = b[num];
					num++;
				}
				while ((num < size) && (cl - 1 >= sp)) {
					cl--;
					a[rw][cl] = b[num];
					num++;
				}
				while ((num < size) && (rw - 1 >= sp)) {
					rw--;
					a[rw][cl] = b[num];
					num++;
				}
				delete[] b;
			}
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					cout.width(5);
					cout << a[i][j] << " ";
				}
				cout << endl;
			}
			for (int i = 0; i < row; i++)
				delete[] a[i];
			delete[] a;
		}



	}

	if (fl == 1)	cout << "An error has occurred while reading input data";



	cin.get();
	cin.get();
}
