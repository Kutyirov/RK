#include <iostream>
#include <string>
using namespace std;
int main() {
	int raz, fl = 1, k = 0;
	cin >> raz;
	char c;
	cin >> c;
	if (raz > 0) {
		int* a = new int[raz];
		for (int i = 0; i < raz; i++) {
			a[i] = 0;
		}
		while (c != '\n') {
			if (c == ' ') {
				k++;
			}
			else {
				a[k] = a[k] * 10 + c - 48;
			}
			cin.get(c);
		}
		if ((raz > 0) && (k == raz - 1)) {
			for (int i = 0; i < raz / 2; i++) {
				swap(a[i], a[raz - 1 - i]);
			}
			for (int i = 0; i < raz; i++) {
				cout << a[i] << " ";
			}
		}
		else
			cout << "An error has occurred while reading input data";
	}
	cin.get();
}
